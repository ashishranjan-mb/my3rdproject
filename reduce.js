function reduce(elements, cb, initialValue) {
    
   if(elements.length === 0) {
       return [];
   }
   for(let i=0;i<elements.length;i++){
       if(initialValue === undefined){
           initialValue = elements[0];
       }
       initialValue = (cb(initialValue, elements[i]))
   }
   return initialValue;
}
module.exports = reduce;