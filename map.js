function map(elements, cb) {
    let res = [];
    for(let i=0;i<elements.length;i++) {
        res.push(cb(elements[i]));
    }
    return res;
}
module.exports = map;